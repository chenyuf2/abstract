import React, {Component} from 'react';
import './home.css';
import { Tween } from 'react-gsap';
export default class Home extends Component {
    componentDidMount = () => {
        // let windowW = parseInt(window.innerWidth) - parseInt(document.getElementById("text").offsetWidth);
        // document.getElementById("text").style.right = windowW / 2 + 'px';
        // console.log(document.getElementById("text").offsetWidth);
    }
    render() {
        return (
        <div className="outer-container">
            <nav className="navbar">
                <Tween
                from={{opacity: '0', y: '20'}} 
                duration={1.3}
                delay={0.5}
                stagger = {0.2}
                >
                <div className="logo">
                    A
                </div>
                <div className="menu">
                    <ion-icon name="ios-menu"></ion-icon>
                </div>
                <div className="lang">
                    eng
                </div>
                <div className="search">
                    <ion-icon name="ios-search"></ion-icon>
                </div>
                </Tween>
            </nav>
          <div className="left-part">
          </div>
          <div className="right-part">
          </div>
          <div className="overlay-right">
            <div id="text">
            <svg  version="1.1" id="Layer_1" xmlns="&ns_svg;"  width="460.5" height="531.74"
                    viewBox="0 0 460.5 531.74" overflow="visible" enable-background="new 0 0 460.5 531.74">
                <polygon stroke="#000000" points="460,530.874 1,265.87 460,0.866 "/>
                </svg>
                ABSTRACTION
                <svg  fill="white" version="1.1" id="Layer_1" xmlns="&ns_svg;"  width="460.5" height="531.74"
                    viewBox="0 0 460.5 531.74" overflow="visible" enable-background="new 0 0 460.5 531.74">
                <polygon stroke="#000000" points="0.5,0.866 459.5,265.87 0.5,530.874 "/>
                </svg>
                </div>
          </div>
          <div className="title">
              <h1>
              <svg  version="1.1" id="Layer_1" xmlns="&ns_svg;"  width="460.5" height="531.74"
                    viewBox="0 0 460.5 531.74" overflow="visible" enable-background="new 0 0 460.5 531.74">
                <polygon stroke="#000000" points="460,530.874 1,265.87 460,0.866 "/>
                </svg>
                  <span>ABSTRACTION</span>
                  <svg  fill="white" version="1.1" id="Layer_1" xmlns="&ns_svg;"  width="460.5" height="531.74"
                    viewBox="0 0 460.5 531.74" overflow="visible" enable-background="new 0 0 460.5 531.74">
                <polygon stroke="#000000" points="0.5,0.866 459.5,265.87 0.5,530.874 "/>
                </svg>
                  </h1>
          </div>
          <Tween
          from={{opacity: '0', y: '20'}} 
          duration={1.3}
          delay={0.8}
          >
          <div className="art-title">
              Art
          </div>
          </Tween>
          <div className="menu-title">
          <Tween
          from={{opacity: '0', y: '20'}} 
          duration={1.3}
          delay={1}
          stagger ={0.1}
          >
              <span style={{fontSize: '30px', fontWeight: '400'}}>078</span>
              <span>IN</span>
              <span>FB</span>
              <span>YT</span>
            </Tween>
          </div>
          <div className="page-title">
          <Tween
          from={{opacity: '0', y: '20'}} 
          duration={1.3}
          delay={1.2}
          stagger ={0.15}
          >
            <div className="page-sub" style={{fontSize:"7rem"}}>01</div>
            <div className="page-sub">02</div>
            <div className="page-sub">03</div>
            </Tween>
          </div>
        </div>
        )
    }
}